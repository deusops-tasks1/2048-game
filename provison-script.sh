#!/bin/bash

# Install Nginx
sudo apt update && sudo apt install nginx -y

# Go to vagrant directory
cd /vagrant

# Copy 'proxy-server' file to /etc/nginx/sites-available/proxy-server
sudo cp proxy-server /etc/nginx/sites-available/proxy-server

# Add a link to enable proxy-server
sudo ln -s /etc/nginx/sites-available/proxy-server /etc/nginx/sites-enabled/proxy-server

# Disable the default page by removing it from '/etc/nginx/sites-enabled'
rm /etc/nginx/sites-enabled/default

# Install npm and NodeJS 16
curl -s https://deb.nodesource.com/setup_16.x | sudo bash
sudo apt-get install -y nodejs

# Add execution permissions for the 'start_script.sh'
chmod +x start_script.sh

# Copy the file '2048-game.service' to '/etc/systemd/system' and start the service
sudo cp 2048-game.service /etc/systemd/system
sudo systemctl start 2048-game.service

# Check status of the service
sudo systemctl status 2048-game.service

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl -y
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

# Install the latest Docker
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

# Add user Vagrant to Docker group
sudo usermod -aG docker vagrant
