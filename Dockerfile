FROM node:16.20.2-alpine3.18 AS build-stage
WORKDIR /app
COPY . .
RUN npm install --include=dev && npm run build

FROM nginx:alpine AS prod-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
