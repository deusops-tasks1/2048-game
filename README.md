# Overview

Sample javascript application implementing the classic [2048 game](https://en.wikipedia.org/wiki/2048_(video_game)). Main project is based on the [game-2048 library](https://www.npmjs.com/package/game-2048) and [Webpack](https://webpack.js.org).

## Requirements  
node 16

## Building the 2048 Game Application

Main project is `javascript` based, hence you can build the application via `npm`:

```shell
npm install --include=dev

npm run build
```

You can test the application locally, by running below command:

```shell
npm start
```

Above command will start a web server in development mode, which you can access at [localhost:8080](http://localhost:8080). Please visit the main library [configuration](https://www.npmjs.com/package/game-2048#config) section from the public npm registry to see all available options.

Open a web browser and point to [localhost:8080](http://localhost:8080/). You should see the `game-2048` welcome page:

![2048 Game Welcome Page](assets/images/game-2048-welcome-page.png)

## Tasks

#### Light

1. Use [Vagrant](https://www.vagrantup.com/) to run `game01` VM
```sh
vagrant up
```
Log in to VM
```sh
vagrant ssh
```

2. Install and configure Nginx on `game01` and configure Nginx as a proxy to localhost

Install Nginx and verify the version
```sh
sudo apt update && sudo apt install nginx -y
nginx -v
```
Copy `proxy-server` to `/etc/nginx/sites-available/proxy-server`

Add a link to enable proxy-server
```sh
ln -s /etc/nginx/sites-available/proxy-server /etc/nginx/sites-enabled/proxy-server
```

Disable deafult page
```sh
rm /etc/nginx/sites-enabled/default
```

3. Run the game on VM and configure nginx to open the web page with the game

Install NodeJS 16
```sh
curl -s https://deb.nodesource.com/setup_16.x | sudo bash
sudo apt-get install -y nodejs
```

Go to the project directory and run next commands:
```sh
npm install --include=dev
npm run build
```

Run application
```sh
npm start &
```
![Playing 2048](assets/images/playing-2048.png)

4. Create Linux System Unit that is responsible for starting the game service
Add execution permission for `start_script.sh`

```sh
chmod +x start_script.sh
```

Copy the file `2048-game.service` to `/etc/systemd/system` and start the service

```sh
sudo cp 2048-game.service /etc/systemd/system
sudo systemctl start 2048-game.service
```
Check status of the service
```sh
sudo systemctl status 2048-game.service
```

5. Make a simple automatization to configure a server, either by bash script or by [Vagrant Provisioning](https://developer.hashicorp.com/vagrant/docs/provisioning).

Added additional line to `Vagrantgile` to enable shell privisoning

Simply run
```sh
vagrant up --provision
```
NOTE: If this is the first start of a machine you can simply run `vagrant up`
If this is NOT the first start, then one of the next commands can be used
```sh
vagrant up --provision
vagrant reload --provision
```

#### Normal

1. Write Dockerfile for the game (the project in using node 16).
Make the Docker image lightway using multi-stage build.

2. Install Docker on `game01` and run the game inside a container.
Configure access to the app inside the container without external Nginx.

[Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
Edit `provison-script.sh` to automate this process.

Build Docker image
```sh
docker build -t 2048-game:1.0 .
```

Run Docker container
```sh
docker run -d -p 8081:80 2048-game:1.0
```

3. Write GitLab-CI pipeline to automatically build and publish Docker image with the 2048-game
Create `gitlab-ci.yml` config and push the changes to the GitLab repository.

4. Create Ansible Role `docker` for docker installation process. Role must pass tests of [Ansible Molecule](https://ansible.readthedocs.io/projects/molecule/).

Here I use `molecule version: 5.0.1`

Next commands can be used to install `molecule 5.0.1, molecule-docker`
```sh
pip install molecule==5.0.1
pip install molecule-docker==2.0.
pip show molecule
```
![Molecule in Action](assets/images/molecule-help-output.png)

Command to create a role
```sh
molecule init role -d docker deusops.docker
```

NOTE: This command can be used to fix some issues
```sh
ansible-galaxy collection install community.docker --force
```
Use the provisioner to start the instances
```sh
molecule create
```

Test ansible role using Molecule
```sh
molecule test
```

There is also `gitlab-ci.yml` that will automatically test the role.

5. Write Ansible playbook to configure server `game01` as at level Light 5 using template
system unit that starts Docker container with the application.

Inside `.cicd/secrets` directory there is an ecnrypted by ansible-vault `private_key` which is used by Ansible to connect to a machine by SSH. Ansible playbook in `.cicd` uses `docker` role.
To run ansible do next steps
```sh
cd .cicd
ansible-playbook --ask-vault-pass playbook.yml
```
Enter Ansible Vault password when prompt appears.

6. Add a step to GitLab-CI to deploy the application on servers. Ansible roles must be in their own git repositories and work through ansible-galaxy.

One server with private key has been created in Cloud. Deploy stage is added to the `gitalb-ci.yml`

NOTE: Usually, you would run ansible playbook on a configured and registered runner with all the required tools, like ansible, ssh, already installed. Here I deploy the application 
running a container from my Docker image based on Ubuntu 22.04 with installed Python 3 and pip. I do this for testing purposes. For storing ansible vault password I am using GitLab
variable to decrypt private SSH key. Variable's flags are shown below.

![Variable's Flags](assets/images/var_flags.png)

##### 🚨 It is not the best way to store passwords and private keys! The better options are:
- [Hashicorp Vault](https://www.hashicorp.com/products/vault)
- [AWS Key Management Service](https://aws.amazon.com/kms/)
- [Google Cloud Key Management](https://cloud.google.com/security/products/security-key-management)
- [Ansible Vault](https://docs.ansible.com/ansible/latest/vault_guide/index.html)

7. Increase the number of servers to 3 and configure balancing of incoming requests using [HAProxy](https://docs.haproxy.org/2.4/intro.html)

Digital Ocean cloud is used to complete this task.

First of all, I need to create the infrastructue: 2 servers for the 2048-game application and 1 server for HAproxy.
To create droplets on Digital Ocean I use my role [digital_ocean_droplet](https://docs.haproxy.org/2.4/intro.html)

I also use my role [haproxy](https://gitlab.com/deusops-tasks1/ansible-roles/ansible-role-haproxy) to install and configure haproxy.

I have 2 playbooks:
- **infrastructure.yml** to configure the infrastructure on Digital Ocean
- **application.yml** to configure application and haproxy on Digital Ocean droplets

To make the process even more automated according to [Best Practices](https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#use-dynamic-inventory-with-clouds)
dynamic inventory `digitalocean.yml` is used to configure hosts dynamically.

NOTE: Before running the playbook we need to make sure environment variable `DO_API_TOKEN` must be set to grant access from runner (Ansible control node) to Digital Ocean API and all required roles are downloaded. Then run the playbooks to configure infrastructure and application on Digital Ocean automatically.
```sh
ansible-galaxy install -f -r requirements.yml
ansible-playbook playbooks/infrastructure.yml
ansible-playbook playbooks/application.yml
```
To automate it even further, I changed `gitlab-ci.yml` file, so the runner will configure everything on Digital Ocean automatically from nothing.
